/*import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';*/

import {useState, useContext} from "react";
import {NavLink} from "react-router-dom"

import {Container, Nav, Navbar} from "react-bootstrap";
import UserContext from "../UserContext";


export default function AppNavbar(){
	
	// create a user state that will be used for the conditional rendering of our nav bar.
	
	const {user} = useContext(UserContext);
	console.log(user)




	return(
	<Navbar bg="light" expand="lg" sticky="top">
	      <Container fluid>
	        <Navbar.Brand as = {NavLink} to = "/">Zuitt</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="ms-auto">
	            <Nav.Link as = {NavLink} to = "/" end>Home</Nav.Link>
	            <Nav.Link as = {NavLink} to = "/courses" end>Courses</Nav.Link>
	            {
	            	(user.id !== null)
	            	?
	            		<Nav.Link as = {NavLink} to = "/logout" end>Logout</Nav.Link>
	            	:
	            	<>
	            		<Nav.Link as = {NavLink} to = "/login" end>Login</Nav.Link>
	           			<Nav.Link as = {NavLink} to = "/register" end>Register</Nav.Link>
	            	</>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>

		)
}