import {Card} from "react-bootstrap"
import {useState, useEffect} from "react";
import {Button} from "react-bootstrap";

import{Link} from "react-router-dom"
export default function CourseCard({courseProp}){
	/*console.log(props.courseProp.name)*/

	const { _id, name, description, price, slots} = courseProp;

	// const [count, setCount] = useState(0);
	/*console.log(useState(10))*/
	// const[seat, setSeat] = useState(30);
	// function enroll(){
	// 	if(seat === 0){
	// 		return alert("No more seats!")
	// 	}
		
	// 	else{
	// 		console.log(seat)
	// 		setSeat(seat - 1);
	// 		setCount(count + 1)
	// 	}
	// }
		return(
		<Card className = "my-3">
      
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>
          Php {price}
        </Card.Text>
        <Card.Subtitle>Slots:</Card.Subtitle>
        <Card.Text>
        	{slots} Available
        </Card.Text>
        <Button as = {Link} to = {`/courses/${_id}`} variant="primary" >Details</Button>
      </Card.Body>
    </Card>
		)
}