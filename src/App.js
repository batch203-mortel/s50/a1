import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import './App.css';
import AppNavbar from "./components/AppNavbar";
import {Container} from "react-bootstrap";

import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import PageNotFound from "./pages/PageNotFound";
import CourseView from "./pages/CourseView"
import {useState, useEffect} from 'react';

import {UserProvider} from "./UserContext";


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null});
  
  const unsetUser = () => {localStorage.clear()}

  useEffect(()=>{
    console.log(user);
    console.log(localStorage)}, [user])

    // to update the User state upon a page load is initiated and a user already exists.
  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}users/details`,{
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(response => response.json())
    .then(data => {
      console.log(data)
      if(data._id !== undefined){
           // Change the global "user" state to store the "id" and "isAdmin"
          setUser({
             id: data._id,
             isAdmin: data.isAdmin
      })
      }
       else{
           setUser({
             id: null,
             isAdmin: null
                 })
          }
    })
  }, [])


  return (
    // we store information in the context by providing the information using the "UserProvider" component and passing the information via the  "value" prop.
      // All the "information" inside the value prop will be accessible to pages/components wrapped around with the UserProvider.

    <UserProvider value={{user, setUser, unsetUser}}>
    {/*/*Router component is use to wrapped around all components which will have access to the routing system.*/}

    <Router>

      <AppNavbar/>
      <Container>
        <Routes>
          <Route exact path = "/" element = {<Home/>}/>
          <Route exact path = "/courses" element = {<Courses/>}/>
          <Route exact path = "/courses/:courseId" element = {<CourseView/>}/>
          <Route exact path = "/login" element = {<Login/>}/>
          <Route exact path = "/logout" element = {<Logout/>}/>
          <Route exact path = "/register" element = {<Register/>}/>
          <Route path = "*" element = {<PageNotFound/>}/>
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
