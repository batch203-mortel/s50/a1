// We will use react context API to give login user object to have "global " scope  within our application.

import React from "react";

// Create a context object
// A context object with object data type that can be used to store information that can be share to other components within app.
const UserContext = React.createContext();

//The Provider component allows other components to consume or use the context objecy and supply the necessary information needed in the context object.

export const UserProvider = UserContext.Provider;

export default UserContext;


