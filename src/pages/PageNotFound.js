import Banner from "../components/Banner"

const data ={
	title:"404 - Not found",
	description: "The page you are looking for cannot be found!",
	destination: "/",
	label:"Go back to Home!"
	};

export default function PageNotFound(){
	
	
		return(
        		<Banner bannerProp={data}/>
		)
}