import Highlights from "../components/Highlights"
import Banner from "../components/Banner"


const data = {
		title:"Zuitt Coding Bootcamp",
		description: "Opportunities for everyone, everywhere!",
		destination: "/courses",
		label:"Enroll Now!"
			};


export default function Home(){
	return(
		<>
        		<Banner bannerProp={data}/>
       			<Highlights/>
       			
		</>
		
		)
}