import {Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from "../UserContext";
import Swal from "sweetalert2";
	

export default function Login(){
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// allow us to consume the User Context object and its values(properties) to use for user validation.
	const {user, setUser} = useContext(UserContext);
	const [isActive, setIsActive] = useState(false);
	/*const navigate = useNavigate();*/
	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else
		{
			setIsActive(false)
		}
	}, [email, password])


	function loginUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}users/login`, {
			method: "POST",
			headers: {
				"Content-Type":"Application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(response => response.json())
		.then(data => {console.log(data.accessToken);
				if(data.accessToken !== undefined){
					localStorage.setItem("token", data.accessToken);
					
					retrieveUserDetails(data.accessToken)

					Swal.fire({
						title: "Login Successful!",
						icon: "success",
						text: "Welcome to Zuitt!"
					})
				}
				else{
					Swal.fire({
						title: "Authentication failed!",
						icon: "error",
						text: "Check your login details and try again"
					})
				}
		})


		const retrieveUserDetails = (token) =>{
			// Token will be sent as part of the request's header.

			fetch(`${process.env.REACT_APP_API_URL}users/details`,{
				headers: {
					Authorization: `Bearer ${token}`
				}
			}).then(response => response.json()).then(data => {console.log(data);
					// Change the global "user" state to store the "id" and "isAdmin"
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

			})
			
					}
		
		// localStorage.setItem("email", email);
		
		// setUser({
		// email: localStorage.getItem("email")
		// })
		
		
		setEmail('');
		setPassword('');

		/*alert("Congrats, successfully logged in!")*/
		/*navigate("/");*/
	}


	return(
		(user.id !== null)
		?
			<Navigate to = "/courses"/>
		:
			<>
			<h1 className = "my-5 text-center">Login</h1>
			<Form className = "mb-5"onSubmit = {e => loginUser(e)}>
		      <Form.Group className="mb-3" controlId="email">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter email" 
		        	required
		        	value = {email}
		        	onChange = {e => setEmail(e.target.value)}/>
		        <Form.Text className="text-muted">
		          Use Email that is registered to the website.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password" 
		        	required
		        	value = {password}
		        	onChange = {e => setPassword(e.target.value)}/>
		      </Form.Group>
		     
		      <Form.Group className="mb-3" controlId="formBasicCheckbox">
		        <Form.Check type="checkbox" label="Remember Me" />
		      </Form.Group>
		     {
		     	isActive ? <Button variant="primary" type="submit" id = "loginBtn">Login</Button> :
		     	<Button variant="danger" type="submit" id = "loginBtn" disabled>Login</Button>
		     }
					      
			</Form>
		</>
		)
}